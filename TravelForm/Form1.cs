﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxCountry.SelectedIndex = 0;
            labelResult.Visible = false;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void buttonCount_Click(object sender, EventArgs e)
        {
            uint days, result = 0;
            if (!(uint.TryParse(textBoxDays.Text, out days)) || days < 1)
            {
                MessageBox.Show("Помилка при введенні кількості днів!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (comboBoxCountry.SelectedIndex == 0 && radioButtonSummer.Checked == true)
                result = days * 100;
            if (comboBoxCountry.SelectedIndex == 0 && radioButtonWinter.Checked == true)
                result = days * 150;
            if (comboBoxCountry.SelectedIndex == 1 && radioButtonSummer.Checked == true)
                result = days * 160;
            if (comboBoxCountry.SelectedIndex == 1 && radioButtonWinter.Checked == true)
                result = days * 200;
            if (comboBoxCountry.SelectedIndex == 2 && radioButtonSummer.Checked == true)
                result = days * 120;
            if (comboBoxCountry.SelectedIndex == 2 && radioButtonWinter.Checked == true)
                result = days * 180;
            if (checkBoxGuide.Checked == true)
                result += days*50;
            labelResult.Visible = true;
            labelResult.Text = "$ " + result.ToString();
        }
    }
}
