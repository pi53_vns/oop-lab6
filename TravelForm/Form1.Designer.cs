﻿namespace TravelForm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxGuide = new System.Windows.Forms.CheckBox();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.textBoxDays = new System.Windows.Forms.TextBox();
            this.radioButtonSummer = new System.Windows.Forms.RadioButton();
            this.radioButtonWinter = new System.Windows.Forms.RadioButton();
            this.buttonCount = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Країна:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(188, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "К-сть днів подорожі:";
            // 
            // checkBoxGuide
            // 
            this.checkBoxGuide.AutoSize = true;
            this.checkBoxGuide.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxGuide.Location = new System.Drawing.Point(53, 165);
            this.checkBoxGuide.Name = "checkBoxGuide";
            this.checkBoxGuide.Size = new System.Drawing.Size(188, 26);
            this.checkBoxGuide.TabIndex = 3;
            this.checkBoxGuide.Text = "Індивідуальний гід";
            this.checkBoxGuide.UseVisualStyleBackColor = true;
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCountry.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCountry.FormattingEnabled = true;
            this.comboBoxCountry.Items.AddRange(new object[] {
            "Болгарія",
            "Німеччина",
            "Польща"});
            this.comboBoxCountry.Location = new System.Drawing.Point(230, 37);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(175, 30);
            this.comboBoxCountry.TabIndex = 4;
            // 
            // textBoxDays
            // 
            this.textBoxDays.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDays.Location = new System.Drawing.Point(230, 79);
            this.textBoxDays.Name = "textBoxDays";
            this.textBoxDays.Size = new System.Drawing.Size(175, 29);
            this.textBoxDays.TabIndex = 5;
            // 
            // radioButtonSummer
            // 
            this.radioButtonSummer.AutoSize = true;
            this.radioButtonSummer.Checked = true;
            this.radioButtonSummer.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonSummer.Location = new System.Drawing.Point(35, 124);
            this.radioButtonSummer.Name = "radioButtonSummer";
            this.radioButtonSummer.Size = new System.Drawing.Size(63, 26);
            this.radioButtonSummer.TabIndex = 6;
            this.radioButtonSummer.TabStop = true;
            this.radioButtonSummer.Text = "Літо";
            this.radioButtonSummer.UseVisualStyleBackColor = true;
            // 
            // radioButtonWinter
            // 
            this.radioButtonWinter.AutoSize = true;
            this.radioButtonWinter.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonWinter.Location = new System.Drawing.Point(147, 124);
            this.radioButtonWinter.Name = "radioButtonWinter";
            this.radioButtonWinter.Size = new System.Drawing.Size(72, 26);
            this.radioButtonWinter.TabIndex = 7;
            this.radioButtonWinter.Text = "Зима";
            this.radioButtonWinter.UseVisualStyleBackColor = true;
            // 
            // buttonCount
            // 
            this.buttonCount.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCount.Location = new System.Drawing.Point(35, 219);
            this.buttonCount.Name = "buttonCount";
            this.buttonCount.Size = new System.Drawing.Size(206, 34);
            this.buttonCount.TabIndex = 8;
            this.buttonCount.Text = "Розрахувати";
            this.buttonCount.UseVisualStyleBackColor = true;
            this.buttonCount.Click += new System.EventHandler(this.buttonCount_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(31, 283);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 22);
            this.label2.TabIndex = 9;
            this.label2.Text = "Вартість:";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.labelResult.ForeColor = System.Drawing.Color.DarkRed;
            this.labelResult.Location = new System.Drawing.Point(155, 283);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(105, 22);
            this.labelResult.TabIndex = 10;
            this.labelResult.Text = "Результат";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel1.Location = new System.Drawing.Point(230, 315);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(196, 22);
            this.linkLabel1.TabIndex = 11;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Волинець Н.С., ПІ-53";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 346);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonCount);
            this.Controls.Add(this.radioButtonWinter);
            this.Controls.Add(this.radioButtonSummer);
            this.Controls.Add(this.textBoxDays);
            this.Controls.Add(this.comboBoxCountry);
            this.Controls.Add(this.checkBoxGuide);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 400);
            this.MinimumSize = new System.Drawing.Size(450, 370);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тури";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxGuide;
        private System.Windows.Forms.ComboBox comboBoxCountry;
        private System.Windows.Forms.TextBox textBoxDays;
        private System.Windows.Forms.RadioButton radioButtonSummer;
        private System.Windows.Forms.RadioButton radioButtonWinter;
        private System.Windows.Forms.Button buttonCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

