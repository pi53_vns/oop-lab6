﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {            
            comboBoxMaterial.SelectedIndex = 0;
            labelResult.Visible = false;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void buttonCount_Click(object sender, EventArgs e)
        {  
            double width, height, result = 0, area;
            if (!(double.TryParse(textBoxWidth.Text, out width)) || width <= 0)
            {
                MessageBox.Show("Помилка при введенні значення ширини!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!(double.TryParse(textBoxHeight.Text, out height)) || height <= 0)
            {
                MessageBox.Show("Помилка при введенні значення висоти!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            area = width * height;            
            if (comboBoxMaterial.SelectedIndex == 0 && radioButtonOneCam.Checked == true)
                result = area * 0.25;
            if (comboBoxMaterial.SelectedIndex == 0 && radioButtonTwoCams.Checked == true)
                result = area * 0.30;
            if (comboBoxMaterial.SelectedIndex == 1 && radioButtonOneCam.Checked == true)
                result = area * 0.05;
            if (comboBoxMaterial.SelectedIndex == 1 && radioButtonTwoCams.Checked == true)
                result = area * 0.10;
            if (comboBoxMaterial.SelectedIndex == 2 && radioButtonOneCam.Checked == true)
                result = area * 0.15;
            if (comboBoxMaterial.SelectedIndex == 2 && radioButtonTwoCams.Checked == true)
                result = area * 0.20;
            if (checkBoxSill.Checked == true)
                result += 35;
            labelResult.Visible = true;
            labelResult.Text = result.ToString("F3")+" грн.";
        }
    }
}
